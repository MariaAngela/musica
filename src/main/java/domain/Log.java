package domain;

import java.sql.Date;

public class Log implements BaseEntity<Long> {
	
	private static final long serialVersionUID = 1L;
	
	public final static String TABLE_NAME = "Logs_administrador";
	
	private Date date;
	private Long id_administrador;
	
	
	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}

	public Long getId_administrador() {
		return id_administrador;
	}


	public void setId_administrador(Long id_administrador) {
		this.id_administrador = id_administrador;
	}


	public Long getId() {
		return null;
	}

	public void setId(Long id) {
	}
}