package domain;

 
public class LogAdmi implements BaseEntity<Long> {
	
	private static final long serialVersionUID = 1L;
	
	public final static String TABLE_NAME = "Logs_administrador";
	
	private Long date;
	private Long id_administrador;
	
	public Long getDate() {
		return date;
	}


	public void setDate(Long date) {
		this.date = date;
	}

	public Long getId_administrador() {
		return id_administrador;
	}


	public void setId_administrador(Long id_administrador) {
		this.id_administrador = id_administrador;
	}

	
	public Long getId() {
		return null;
	}

	public void setId(Long id) {
	}
}