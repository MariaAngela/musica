package domain;

public class Puntuacion implements BaseEntity<Long>{
	private static final long serialVersionUID = 1L;
	public final static String TABLE_NAME = "Puntuaciones";
	
	private Long idContent;
	private Long idUser;
	private Integer val; 

	public Long getIdContent() {
		return idContent;
	}

	public void setIdContent(Long idContent) {
		this.idContent = idContent;
	}

	public Long getIdUser() {
		return idUser;
	}

	public void setIdUser(Long idUser) {
		this.idUser = idUser;
	}
	
	public Integer getVal() {
		return val;
	}
	
	public void setVal(Integer val) {
		this.val = val;
	}

	public Long getId() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setId(Long id) {
		// TODO Auto-generated method stub
		
	}
}