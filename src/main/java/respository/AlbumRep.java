package respository;

import domain.Album;

public interface AlbumRep extends GenericRep<Album, Long>{

}