package respository;

import java.util.List;

import domain.Administrador;

public interface LogAdmiRep {
	
	List<Administrador> filterByAdministrator(int id_admi);
	
}