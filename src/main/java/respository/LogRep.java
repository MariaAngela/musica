package respository;

import java.util.List;

import domain.Log;

public interface LogRep  extends GenericRep<Log, Long> {
	
	List<Log> filterByAdministrator(Long idAdministrator);

	
}