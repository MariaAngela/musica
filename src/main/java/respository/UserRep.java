package respository;

import java.util.List;

import domain.User;

public interface UserRep extends GenericRep<User, Long> {
	
	User findByEmail(String email);
	Long findIdByUsername(String username);

	List<User> filterByEmail(String email);
	void updateBalance(User user);
	
	boolean existsUser(String username, String password);
	void charge(Long idUser, Long charge);
}