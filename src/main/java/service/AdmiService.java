package service;

import java.util.List;

import domain.Administrador;

public interface AdmiService {
	Administrador findByEmail(String email);
	List<Administrador> filterByEmail(String email);
}
