package service;

import java.util.List;

import domain.Log;

public interface LogService {
	List<Log> filterByAdministrador(Long idAdministrador);
	
}
