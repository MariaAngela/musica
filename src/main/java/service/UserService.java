package service;

import domain.User;

public interface UserService {

	User findByEmail(String email);
}